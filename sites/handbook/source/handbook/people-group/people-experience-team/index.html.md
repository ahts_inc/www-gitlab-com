---
layout: handbook-page-toc
title: "People Experience Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# People Experience Team

## People Experience Team Availability

Holidays with no availability for onboarding/offboarding/career mobility issues:

| Date                        |
|-----------------------------------|
| 2020-05-01 |
| 2020-12-25 |
| 2021-01-01 |

### OOO Handover Process for People Experience Team

1. The People Experience Associate will create a document the day before their scheduled OOO with the following:
- What urgent tasks are there that need to be reassigned:
* Onboarding
* Offboarding
* Career Mobilities
* Any other tasks
- Is there anything else that we should be made aware of and contribute to whilst you are away?
2. The People Experience Coordinator will then reassign tasks to an alternative People Experience Associate.
3. Get assistance from the People Operation Specialist team if additional hands are needed.

### OOO Process for People Experience Coordinator

1. When the People Experience Coordinator is due to be OOO, the People Experience Associates will allocate the relevant tasks between themselves.
2. In the event that there are items that need urgent attention whilst the People Experience Coordinator is OOO, the Coordinator will hand over the relevant tasks to the People Experience Team Lead or People Experience Associates.

## People Experience Team Processes

### Weekly Rotations 

The People Experience Team are currently trialling a task rotation on a weekly basis. The allocation tracker can be found in the [People Exp/Ops Tracker](https://docs.google.com/spreadsheets/d/1Z45eOZ2rCRIKgGae3eOKQ8lhIbAPikSs5gLz3V8Kh9U/edit?ts=5e7a2c42#gid=2024040529). This will initially be trialled on a weekly rotation and then possibly moved to monthly. 

A task will be allocated based on a ongoing team member rotation between all People Experience Associates. 

The following factors will also be taken into consideration:

- Scheduled PTO for the team members
- Ensure that the tasks split evenly and fairly 
- A tracker will be kept to track the data of how the tasks are split up

The Rotation groups are listed as follows:

- Letters of Employment / VOE / OSAT Feedback
- Probation / Anniversary Queries / Gift Requests
- BHR Onboarding Report / Moo Report
- Allocations for Onboarding / Offboarding / Mobility

#### Allocations for Onboarding

- We always try and split evenly and fairly
-  

#### Allocations for Offboarding

- Team has 24 hours to create and notify of the offboarding issue
- The person that is in the allocation rotation will also add the offboarding to the PEA team calendar

#### Allocations for Career Mobility 

### Audits 

- Onboarding
- Offboarding
- Transition
- Probation Period
- Referrals

### Pulling of BambooHR Onboarding Data

- Report is pulled every Monday and Wednesday
Please list steps here for the process :) 

### Anniversary Period Gift Queries

Process will be included here (current and potentially new process)

### Gift Requests

When a team member completes the GitLab gift form request, the People Experience team receives an email to people-exp@domain.com to process the request. Most often, these are requests for flowers to be sent to another team member. Please see the below steps for guidance on how to process these requests:

- Navigate and open the gift form requests in `Google spreadsheet`. 
- Open the PeopleOps 1Password Vault and select `Gift & Flower Vendors` to gain access to the various vendors used. 
- Place order and once confirmed, add data, including order confirmation link, to spreadsheet. 
- Send the requesting team member an email or message in Slack to confirm that you have processed the request/order. 
- Use the Gift [page](https://about.gitlab.com/handbook/people-group/#gifts) in the Handbook for any further information regarding the policy for gift order requests. 

### HelloSign

As the DRI for HelloSign, when a team member needs to have access to sign documents for the company, an access request needs to be created and assigned to the People Experience Team for provisioning. 

#### Process once Access Request is received:

1. Log into [HelloSign](https://app.hellosign.com/home/) using your personal account information
1. Select `Team` on the left hand-side of the page
1. Insert the team members GitLab email address and click `Invite`
1. Take a screenshot of the confirmed invitation sent and upload to the Access Request as confirmation

#### Monthly Billing

1. When monthly expenses are due, we need to be able to provide Finance with the specific team members name in order to assign to the correct Department. To do this we would need to send a request to HelloSign Support on a monthly basis and often the response is not received in time for the monthly expenses. Once the list is received from HelloSign, simply send the Accounts Payable Specialist an email with the information.  

#### 1Password Recovery Emails

As we have admin rights to 1Password, on occassion we receive emails to people-exp@domain.com from 1Password with the subject line `Recovery has been continued!` to assist with the recovery of a team members 1Password access. Simply ignore and delete this email, as the IT Ops Team will assist with the recovery. 
