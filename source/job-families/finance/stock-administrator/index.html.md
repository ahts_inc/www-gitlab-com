---
layout: job_family_page
title: "Stock Administrator"
---

## Role Overview

Responsible for administering GitLab’s equity incentive program which includes option and RSU grants to team members. Process transactions with Carta (or similar system) for employee grants, exercises, repurchases, cancellations and handles other duties related to employees and contractors stock options and other equity grants. Will be diligent in answering employee questions and resolves problems regarding equity plans and grants. Responsible for documenting and improving the company’s policies and processes in the GitLab Handbook. Maintains employee equity database, updating information and running required and requested summary and detail reports. Prepares and distributes grant documentation and agreements for new-hire and ongoing grants electronically. Works on special initiatives related to equity granting and reporting including tax, compensation and accounting projects.

## Responsibilities

* Support day-to-day administration of the company’s equity programs, including processing new equity awards, exercises, cancellations and repurchases.
* Maintain data in Carta online database to ensure accuracy of participant information.
* Respond to employee inquiries regarding equity programs and education while providing excellent service to plan participants at all levels.
* Collaborate with PeopleOperations, Legal and Payroll teams on equity related items as needed.
* Prepare reports for internal partners and respond to ad hoc data requests.
* Assist with development and delivery of employee trainings and communications.
* Will assist in the future with preparation and filing of Section 16 reports (Form 3, 4 and 5) and tracking of executive 10b5-1 plans.
* Lead role in setting up and administering ESPP plans, if applicable, including enrollment, contribution changes and semi-annual purchases.
* Responsible for the tracking and reporting of 83-B information and reporting requirements.
* Owns responsibility for ensuring compliance with US and international tax laws regarding equity incentive.

## Requirements

* Considered a specialist in the field within the function.
* Demonstrates an ability to coordinate multiple projects simultaneously.
* A seasoned, experienced professional with a full understanding of area of specialization; resolves a wide range of issues in creative ways.
* Demonstrates further technical development and a track record of project success.
* 3-5 years of relevant equity administration experience on the Carta platform.
* Experience with both Option and RSU programs.
* Familiarity with U.S. federal and state taxation relating to equity compensation.
* Familiarity with International tax law relating to equity programs.
* Strong analytical, communication and organization skills.
* Prior responsibility working with Big 4 CPA firms in connection with audits.
* Sensitivity to employee privacy laws such as GDPR and ability to maintain strict confidentiality.
* Ability to interact with employees at all levels.
* Experience with international stock options and equity grants.
* CEP Level II Completion (or in process of obtaining CEP Level II).
* Experience with Online systems.
* Ability to use GitLab

## Performance Indicators

* [Number of award transactions processed](/handbook/stock-options/#number-of-award-transactions-processed)
* [Number of participants supported](/handbook/stock-options/#number-of-participants-supported)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Principal Accounting Officer
- Next, candidates will be invited to schedule a 45 minute interview with our Chief Financial Officer
- Candidates will then be invited to schedule a 30 minute interview with our Compensation & Benefits Manager
- Candidates will then be invited to schedule a 30 minute interview with our People Business Partner
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
